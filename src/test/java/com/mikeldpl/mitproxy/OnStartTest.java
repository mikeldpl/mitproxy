package com.mikeldpl.mitproxy;


import com.mikeldpl.mitproxy.util.InitialContextFactoryMockRule;
import org.junit.Rule;
import org.junit.Test;

import javax.naming.Context;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class OnStartTest {

    public ServletContext servletContext = mock(ServletContext.class);
    public Context context = mock(Context.class);

    @Rule
    public InitialContextFactoryMockRule initialContextFactoryMockRule = new InitialContextFactoryMockRule(context);

    @Test
    public void test() throws Exception {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        doReturn(requestDispatcher).when(servletContext).getNamedDispatcher("default");

        doReturn(context).when(context).lookup("java:comp/env");
        String internalServerUrl = "http://internal-server.url";
        doReturn(internalServerUrl).when(context).lookup("com.mikeldpl.mitproxy.configuration.internalServerUrl");
        doReturn("10").when(context).lookup("com.mikeldpl.mitproxy.configuration.http.maxConnections");

        OnStart onStart = new OnStart();
        onStart.onStartup(null, servletContext);
    }
}
