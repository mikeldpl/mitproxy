package com.mikeldpl.mitproxy.util;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.naming.Context;

public class InitialContextFactoryMockRule implements TestRule{
    private final Context context;

    public InitialContextFactoryMockRule(Context context) {
        this.context = context;
    }

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                System.setProperty(Context.INITIAL_CONTEXT_FACTORY, InitialContextFactoryMock.class.getName());
                InitialContextFactoryMock.setCurrentContext(context);
                try {
                    base.evaluate();
                } catch (Exception ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                } finally {
                    System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
                    InitialContextFactoryMock.clearCurrentContext();
                }
            }
        };
    }
}
