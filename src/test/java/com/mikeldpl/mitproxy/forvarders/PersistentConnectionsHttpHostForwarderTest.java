package com.mikeldpl.mitproxy.forvarders;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PersistentConnectionsHttpHostForwarderTest {

    public static final int PORT = 8182;
    public static final String REQUEST_URL = "/servlet/action";
    public static final List<String> REQUEST_HEADER_NAMES = Arrays.asList(new String[]{"Cutom-Header-1"});
    public static final List<String> REQUEST_HEADER_VALUES = Arrays.asList(new String[]{"CUSTOM0---Value"});
    public static final String REQUEST_BODY = "request body ew234t34tg";

    public static final String RESPONSE_BODY = "Response body text";
    public static final String RESPONSE_HEADER_NAME = "custom-header-2";
    public static final String RESPONSE_HEADER_VALUE = "custome34fgrreg+value";
    public static final int RESPONSE_STATUS = 200;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(PORT);

    @Test
    public void testGet() throws Exception {
        URL url = new URL("http://127.0.0.1:" + PORT);
        PersistentConnectionsHttpHostForwarder forwarder = new PersistentConnectionsHttpHostForwarder(true, url);
        forwarder.setHost(url);

        HttpServletRequest request = mock(HttpServletRequest.class);
        doReturn("GET").when(request).getMethod();
        doReturn(Collections.enumeration(REQUEST_HEADER_NAMES)).when(request).getHeaderNames();
        doReturn(Collections.enumeration(REQUEST_HEADER_VALUES)).when(request).getHeaders(REQUEST_HEADER_NAMES.get(0));
        doReturn("param=value").when(request).getQueryString();
        doReturn(REQUEST_URL).when(request).getServletPath();

        HttpServletResponse response = mock(HttpServletResponse.class);
        StringOutputStream os = new StringOutputStream();
        doReturn(os).when(response).getOutputStream();

        WireMock.stubFor(WireMock.get(WireMock
                .urlMatching(REQUEST_URL + ".*"))
                .withHeader(REQUEST_HEADER_NAMES.get(0), WireMock.equalTo(REQUEST_HEADER_VALUES.get(0)))
                .withQueryParam("param", WireMock.containing("a"))
                .willReturn(WireMock.aResponse()
                        .withStatus(RESPONSE_STATUS)
                        .withBody(RESPONSE_BODY)
                        .withHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE)));

        forwarder.forward(request, response);

        Assert.assertEquals(RESPONSE_BODY, os.toString());
        verify(response).addHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE);
    }

    @Test
    public void testGet500() throws Exception {
        URL url = new URL("http://127.0.0.1:" + PORT);
        PersistentConnectionsHttpHostForwarder forwarder = new PersistentConnectionsHttpHostForwarder(true, url);

        HttpServletRequest request = mock(HttpServletRequest.class);
        doReturn("GET").when(request).getMethod();
        doReturn(REQUEST_URL).when(request).getServletPath();
        doReturn(Collections.emptyEnumeration()).when(request).getHeaderNames();

        HttpServletResponse response = mock(HttpServletResponse.class);
        StringOutputStream os = new StringOutputStream();
        doReturn(os).when(response).getOutputStream();

        WireMock.stubFor(WireMock.get(WireMock
                .urlEqualTo(REQUEST_URL))
                .willReturn(WireMock.aResponse()
                        .withStatus(500)
                        .withBody(RESPONSE_BODY)
                        .withHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE)));

        forwarder.forward(request, response);

        Assert.assertEquals(RESPONSE_BODY, os.toString());
        verify(response).addHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE);
    }

    @Test
    public void testPost() throws Exception {
        URL url = new URL("http://127.0.0.1:" + PORT);
        PersistentConnectionsHttpHostForwarder forwarder = new PersistentConnectionsHttpHostForwarder(true, url);

        HttpServletRequest request = mock(HttpServletRequest.class);
        doReturn("POST").when(request).getMethod();
        doReturn(Collections.enumeration(REQUEST_HEADER_NAMES)).when(request).getHeaderNames();
        doReturn(Collections.enumeration(REQUEST_HEADER_VALUES)).when(request).getHeaders(REQUEST_HEADER_NAMES.get(0));
        doReturn(new StringInputStream(REQUEST_BODY)).when(request).getInputStream();
        doReturn(REQUEST_URL).when(request).getServletPath();

        HttpServletResponse response = mock(HttpServletResponse.class);
        StringOutputStream os = new StringOutputStream();
        doReturn(os).when(response).getOutputStream();

        WireMock.stubFor(WireMock.post(WireMock
                .urlEqualTo(REQUEST_URL))
                .withHeader(REQUEST_HEADER_NAMES.get(0), WireMock.equalTo(REQUEST_HEADER_VALUES.get(0)))
                .withRequestBody(WireMock.equalTo(REQUEST_BODY))
                .willReturn(WireMock.aResponse()
                        .withStatus(RESPONSE_STATUS)
                        .withBody(RESPONSE_BODY)
                        .withHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE)));

        forwarder.forward(request, response);

        Assert.assertEquals(RESPONSE_BODY, os.toString());
        verify(response).addHeader(RESPONSE_HEADER_NAME, RESPONSE_HEADER_VALUE);
    }

    private class StringOutputStream extends ServletOutputStream {

        private StringBuilder stringBuilder = new StringBuilder();

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }

        @Override
        public void write(int b) throws IOException {
            stringBuilder.append((char)b);
        }

        @Override
        public String toString() {
            return stringBuilder.toString();
        }
    }

    private class StringInputStream extends ServletInputStream{

        InputStream is;

        public StringInputStream(String content) {
            is = new ByteArrayInputStream(content.getBytes());
        }

        @Override
        public boolean isFinished() {
            return false;
        }

        @Override
        public boolean isReady() {
            return true;
        }

        @Override
        public void setReadListener(ReadListener readListener) {

        }

        @Override
        public int read() throws IOException {
            return is.read();
        }
    }

}
