package com.mikeldpl.mitproxy.notification;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.mockito.internal.matchers.StartsWith;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GlobalErrorHandlerImplTest {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private GlobalErrorHandler globalErrorHandler;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        request = Mockito.mock(HttpServletRequest.class);
        response = Mockito.mock(HttpServletResponse.class);
        globalErrorHandler = new GlobalErrorHandlerImpl();
    }

    @Test
    public void testEmptyHandler() throws IOException {
        globalErrorHandler.addHandler(200, (String) null);
        globalErrorHandler.handleError(200, request, response);
    }

    @Test
    public void testOneHandler() throws IOException {
        ErrorHandler errorHandler = Mockito.mock(ErrorHandler.class);
        globalErrorHandler.addHandler(444, errorHandler);
        globalErrorHandler.handleError(444, request, response);

        Mockito.verify(errorHandler, Mockito.times(1)).handle(request, response);
        Mockito.verify(response, Mockito.times(1)).setStatus(444);
    }

    @Test
    public void testOneHandlerCommittedResponse() throws IOException {
        Mockito.doReturn(true).when(response).isCommitted();

        ErrorHandler errorHandler = Mockito.mock(ErrorHandler.class);
        globalErrorHandler.addHandler(444, errorHandler);
        globalErrorHandler.handleError(444, request, response);

        Mockito.verify(errorHandler, Mockito.times(0)).handle(request, response);
        Mockito.verify(response, Mockito.times(0)).setStatus(444);
    }

    @Test
    public void testRedirect() throws IOException {
        globalErrorHandler.addHandler(300, "redirect:error.html");

        globalErrorHandler.handleError(300, request, response);

        Mockito.verify(response, Mockito.times(1)).sendRedirect("error.html");
        Mockito.verify(response, Mockito.times(1)).setStatus(300);
    }

    @Test
    public void testForward() throws Exception {
        globalErrorHandler.addHandler(302, "forward:error.html");

        RequestDispatcher requestDispatcher = Mockito.mock(RequestDispatcher.class);
        Mockito.doReturn(requestDispatcher).when(request).getRequestDispatcher("error.html");


        globalErrorHandler.handleError(302, request, response);

        Mockito.verify(requestDispatcher, Mockito.times(1)).forward(request, response);
        Mockito.verify(response, Mockito.times(1)).setStatus(302);
    }

    @Test
    public void testForwardException() throws Exception {
        globalErrorHandler.addHandler(302, "forward:error.html");

        RequestDispatcher requestDispatcher = Mockito.mock(RequestDispatcher.class);
        Mockito.doReturn(requestDispatcher).when(request).getRequestDispatcher("error.html");

        Mockito.doThrow(new ServletException()).when(requestDispatcher).forward(request, response);

        expectedException.expect(IOException.class);
        expectedException.expectMessage(new StartsWith("Invalid forward path"));

        globalErrorHandler.handleError(302, request, response);
    }

    @Test
    public void testCustom() throws Exception {
        PrintWriter pw = Mockito.mock(PrintWriter.class);
        Mockito.doReturn(pw).when(response).getWriter();

        globalErrorHandler.addHandler(555, "custom:error.html");

        globalErrorHandler.handleError(555, request, response);

        Mockito.verify(pw, Mockito.times(1)).write("custom:error.html");
    }
}
