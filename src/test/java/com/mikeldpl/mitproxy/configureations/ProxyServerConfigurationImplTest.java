package com.mikeldpl.mitproxy.configureations;

import com.mikeldpl.mitproxy.exceptions.ProxyServerConfigurationException;
import com.mikeldpl.mitproxy.util.InitialContextFactoryMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.mockito.internal.matchers.CompareEqual;
import org.mockito.internal.matchers.Contains;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class ProxyServerConfigurationImplTest {

    private final Context context = Mockito.mock(Context.class);
    private final ServletContext servletContext = mock(ServletContext.class);

    @Rule
    public InitialContextFactoryMockRule initialContextFactoryMockRule = new InitialContextFactoryMockRule(context);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws NamingException {
        doReturn(context).when(context).lookup("java:comp/env");
        String internalServerUrl = "file://internal-server.url";
        doReturn(internalServerUrl).when(context).lookup("com.mikeldpl.mitproxy.configuration.internalServerUrl");
        doReturn("10").when(context).lookup("com.mikeldpl.mitproxy.configuration.http.maxConnections");
    }

    @Test
    public void testWrongDispatcherName() throws Exception {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        doReturn(requestDispatcher).when(servletContext).getNamedDispatcher("WRONG_DISPATCHER_NAME");

        expectedException.expect(ProxyServerConfigurationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Can't init configuration. Default servlet wasn't set."));

        new ProxyServerConfigurationImpl(servletContext);
    }

    @Test
    public void testWrongProtocol() throws Exception {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        doReturn(requestDispatcher).when(servletContext).getNamedDispatcher("default");

        expectedException.expect(ProxyServerConfigurationException.class);
        expectedException.expectMessage(new Contains("It must have some of next protocols"));

        new ProxyServerConfigurationImpl(servletContext);
    }


}
