package com.mikeldpl.mitproxy.configureations;

import com.mikeldpl.mitproxy.util.InitialContextFactoryMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import javax.naming.Context;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(Parameterized.class)
public class ProxyServerConfigurationImplParametrizedTest {

    private final Context context = Mockito.mock(Context.class);
    private final ServletContext servletContext = mock(ServletContext.class);

    @Rule
    public InitialContextFactoryMockRule initialContextFactoryMockRule = new InitialContextFactoryMockRule(context);

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{{"default"}, {"_ah_default"}, {"resin-file"}, {"FileServlet"}, {"SimpleFileServlet"}});
    }

    private String namedDispatcher;

    public ProxyServerConfigurationImplParametrizedTest(String namedDispatcher) {
        this.namedDispatcher = namedDispatcher;
    }

    @Test
    public void test() throws Exception {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        doReturn(requestDispatcher).when(servletContext).getNamedDispatcher(namedDispatcher);

        doReturn(context).when(context).lookup("java:comp/env");
        String internalServerUrl = "http://internal-server.url";
        doReturn(internalServerUrl).when(context).lookup("com.mikeldpl.mitproxy.configuration.internalServerUrl");
        doReturn("10").when(context).lookup("com.mikeldpl.mitproxy.configuration.http.maxConnections");

        ProxyServerConfigurationImpl proxyServerConfiguration = new ProxyServerConfigurationImpl(servletContext);

        assertSame(proxyServerConfiguration.getDefaultServletDispatcher(), requestDispatcher);

        Map<Integer, String> errorsHandlerConfiguration = proxyServerConfiguration.getErrorsHandlerConfiguration();
        assertEquals(errorsHandlerConfiguration.get(404), "redirect:/#/notification/404");
        assertEquals(errorsHandlerConfiguration.get(500), "{\"message\":\"Just error\"}");

        assertEquals(proxyServerConfiguration.getInternalServerUrl(), new URL(internalServerUrl));

        assertEquals(proxyServerConfiguration.isUsingCache(), false);

        Object[] prefixes = proxyServerConfiguration.getPathPrefixes().toArray();

        String[] expectedPrefixes = {"/init", "/public"};
        Arrays.sort(prefixes);
        Arrays.sort(expectedPrefixes);
        assertArrayEquals(prefixes, expectedPrefixes);
    }
}
