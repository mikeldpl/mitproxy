package com.mikeldpl.mitproxy.filters;

import com.mikeldpl.mitproxy.notification.GlobalErrorHandler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.internal.matchers.CompareEqual;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class SessionDisabledFilterTest {
    private GlobalErrorHandler globalErrorHandler;
    private ServletContext servletContext;
    private FilterConfig filterConfig;
    private FilterChain filterChain;
    private HttpServletRequest httpRequest;
    private HttpServletResponse httpResponse;
    private SessionDisabledFiler filter;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        globalErrorHandler = mock(GlobalErrorHandler.class);
        servletContext = mock(ServletContext.class);
        filterConfig = mock(FilterConfig.class);
        httpRequest = mock(HttpServletRequest.class);
        httpResponse = mock(HttpServletResponse.class);

        doReturn(servletContext).when(filterConfig).getServletContext();
        doReturn(globalErrorHandler).when(servletContext).getAttribute(GlobalErrorHandler.class.getName());

        filter = new SessionDisabledFiler();
    }

    @Test
    public void testGetRequestedSession() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper)servletRequest).getRequestedSessionId();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));

        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testGetSession() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper)servletRequest).getSession(true);
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testGetSession2() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper)servletRequest).getSession();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testChangeSessionId() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper) servletRequest).changeSessionId();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testIsRequestedSessionIdValid() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper) servletRequest).isRequestedSessionIdValid();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testIsRequestedSessionIdFromCookie() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper) servletRequest).isRequestedSessionIdFromCookie();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testIsRequestedSessionIdFromURL() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper) servletRequest).isRequestedSessionIdFromURL();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }

    @Test
    public void testIsRequestedSessionIdFromURL2() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletRequestWrapper) servletRequest).isRequestedSessionIdFromUrl();
            }
        };

        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage(new CompareEqual<String>("Http session is unsupported"));
        filter.doFilter(httpRequest, httpResponse, filterChain);
    }
    @Test
    public void testNormalFlow() throws Exception {
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
            }
        };

        filter.doFilter(httpRequest, httpResponse, filterChain);
    }
}
