package com.mikeldpl.mitproxy.filters;

import com.mikeldpl.mitproxy.notification.GlobalErrorHandler;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ErrorPageFilterTest {

    private GlobalErrorHandler globalErrorHandler;
    private ServletContext servletContext;
    private FilterConfig filterConfig;
    private FilterChain filterChain;
    private HttpServletRequest httpRequest;
    private HttpServletResponse httpResponse;

    @Before
    public void setUp() {
        globalErrorHandler = mock(GlobalErrorHandler.class);
        servletContext = mock(ServletContext.class);
        filterConfig = mock(FilterConfig.class);
        filterChain = mock(FilterChain.class);

        httpRequest = mock(HttpServletRequest.class);
        httpResponse = mock(HttpServletResponse.class);

        doReturn(servletContext).when(filterConfig).getServletContext();
        doReturn(globalErrorHandler).when(servletContext).getAttribute(GlobalErrorHandler.class.getName());
    }

    @Test
    public void test() throws Exception {
        ErrorPageFilter filter = new ErrorPageFilter();
        filter.init(filterConfig);
        filter.doFilter(httpRequest, httpResponse, filterChain);

        verify(httpRequest, times(1)).getAttribute(ErrorPageFilter.class.getName());
        verify(httpRequest, times(1)).setAttribute(ErrorPageFilter.class.getName(), Boolean.TRUE);
        verify(filterChain, times(1)).doFilter(same(httpRequest), isA(HttpServletResponseWrapper.class));
    }

    @Test
    public void testDoubleCall() throws Exception {
        doReturn(true).when(httpRequest).getAttribute(ErrorPageFilter.class.getName());
        ErrorPageFilter filter = new ErrorPageFilter();
        filter.init(filterConfig);
        filter.doFilter(httpRequest, httpResponse, filterChain);

        verify(httpRequest, times(1)).getAttribute(ErrorPageFilter.class.getName());
        verify(httpRequest, times(0)).setAttribute(ErrorPageFilter.class.getName(), Boolean.TRUE);
        verify(filterChain, times(1)).doFilter(httpRequest, httpResponse);
    }

    @Test
    public void sendErrorTest() throws Exception {
        final int errorCode = 405;
        filterChain = new FilterChain() {
            @Override
            public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException, ServletException {
                ((HttpServletResponseWrapper) servletResponse).sendError(errorCode, "message");
            }
        };

        ErrorPageFilter filter = new ErrorPageFilter();
        filter.init(filterConfig);
        filter.doFilter(httpRequest, httpResponse, filterChain);

        verify(globalErrorHandler, times(1)).handleError(eq(errorCode),
                any(HttpServletRequest.class),
                any(HttpServletResponse.class));
    }
}
