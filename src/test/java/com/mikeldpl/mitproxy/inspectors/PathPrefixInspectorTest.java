package com.mikeldpl.mitproxy.inspectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RunWith(Parameterized.class)
public class PathPrefixInspectorTest {
    private boolean expected;
    private Set<String> prefixes;
    private String path;

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new String[] {"/a", "/b"}, "/a/bccde", true},
                {new String[] {"/a", "/b"}, "/abccde", false},
                {new String[] {"/c", "/d"}, "/a/bccde", false},
                {new String[] {"/c", "/d"}, "/", false},
                {new String[] {}, "/", false},
        });
    }

    public PathPrefixInspectorTest(String[] prefixes, String path, boolean expected) {
        this.prefixes = new HashSet<>(Arrays.asList(prefixes));
        this.path = path;
        this.expected = expected;
    }

    @Test
    public void test() {
        PathPrefixInspector pathPrefixInspector = new PathPrefixInspector(prefixes);
        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        Mockito.doReturn(path).when(httpServletRequest).getServletPath();

        Assert.assertEquals(pathPrefixInspector.isUnableToForward(httpServletRequest), expected);
    }
}
