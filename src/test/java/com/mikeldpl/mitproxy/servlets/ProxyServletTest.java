package com.mikeldpl.mitproxy.servlets;

import com.mikeldpl.mitproxy.forvarders.HttpHostForwarder;
import com.mikeldpl.mitproxy.inspectors.Inspector;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;

public class ProxyServletTest {

    private ProxyServlet proxyServlet;
    private HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
    private HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
    private HttpHostForwarder forwarder = Mockito.mock(HttpHostForwarder.class);
    private Inspector inspector = Mockito.mock(Inspector.class);
    private RequestDispatcher dispatcher = Mockito.mock(RequestDispatcher.class);

    @Before
    public void setUp(){
        proxyServlet = new ProxyServlet(){
            @Override
            public void init() {
                try {
                    setPrivateField(ProxyServlet.class, this, "httpHostForwarder", forwarder);
                    setPrivateField(ProxyServlet.class, this, "inspector", inspector);
                    setPrivateField(ProxyServlet.class, this, "defaultServletDispatcher", dispatcher);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        };
    }

    @Test
    public void testUnableToForward() throws Exception {
        Mockito.doReturn(false).when(inspector).isUnableToForward(httpServletRequest);
        proxyServlet.init();
        proxyServlet.service(httpServletRequest, httpServletResponse);
        Mockito.verify(dispatcher, Mockito.times(1)).forward(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testForward() throws Exception {
        Mockito.doReturn(true).when(inspector).isUnableToForward(httpServletRequest);
        proxyServlet.init();
        proxyServlet.service(httpServletRequest, httpServletResponse);
        Mockito.verify(forwarder, Mockito.times(1)).forward(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testException() throws Exception {
        Mockito.doReturn(true).when(inspector).isUnableToForward(httpServletRequest);
        Mockito.doThrow(Exception.class).when(forwarder).forward(httpServletRequest, httpServletResponse);
        proxyServlet.init();
        proxyServlet.service(httpServletRequest, httpServletResponse);

        Mockito.verify(httpServletResponse, Mockito.times(1)).sendError(500);
    }

    private static void setPrivateField(Class clazz, Object object, String field, Object value) throws Exception {
        Field f = clazz.getDeclaredField(field);
        f.setAccessible(true);
        f.set(object, value);
    }
}
