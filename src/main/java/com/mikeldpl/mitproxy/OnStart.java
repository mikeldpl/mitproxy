package com.mikeldpl.mitproxy;

import com.mikeldpl.mitproxy.configureations.ProxyServerConfiguration;
import com.mikeldpl.mitproxy.configureations.ProxyServerConfigurationImpl;
import com.mikeldpl.mitproxy.forvarders.HttpHostForwarder;
import com.mikeldpl.mitproxy.forvarders.PersistentConnectionsHttpHostForwarder;
import com.mikeldpl.mitproxy.inspectors.Inspector;
import com.mikeldpl.mitproxy.inspectors.PathPrefixInspector;
import com.mikeldpl.mitproxy.notification.GlobalErrorHandler;
import com.mikeldpl.mitproxy.notification.GlobalErrorHandlerImpl;
import com.mikeldpl.mitproxy.utils.BeansUtil;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.Set;

public class OnStart implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        ProxyServerConfiguration configuration = new ProxyServerConfigurationImpl(ctx);
        BeansUtil.setBean(ctx, ProxyServerConfiguration.class, configuration);
        BeansUtil.setBean(ctx, HttpHostForwarder.class,
                new PersistentConnectionsHttpHostForwarder(configuration.isUsingCache(), configuration.getInternalServerUrl()));
        BeansUtil.setBean(ctx, Inspector.class, new PathPrefixInspector(configuration.getPathPrefixes()));
        BeansUtil.setBean(ctx, GlobalErrorHandler.class,
                new GlobalErrorHandlerImpl(configuration.getErrorsHandlerConfiguration()));
    }
}
