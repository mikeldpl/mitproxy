package com.mikeldpl.mitproxy.utils;

import javax.servlet.ServletContext;

public final class BeansUtil {
    private BeansUtil() {
    }

    public static <T> void setBean(ServletContext ctx, Class<? super T> baseClass, T implementation) {
        ctx.setAttribute(baseClass.getName(), implementation);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(ServletContext ctx, Class<T> baseClass) {
        return (T) ctx.getAttribute(baseClass.getName());
    }
}
