package com.mikeldpl.mitproxy.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class StreamUtil {

    public static final Logger log = Logger.getLogger(StreamUtil.class.getCanonicalName());
    /* 512 kb */
    private static final int BUFFER_DEFAULT_SIZE = 524288;
    private static final ThreadLocal<byte[]> BUFFER_HOLDER = ThreadLocal.withInitial(() -> new byte[BUFFER_DEFAULT_SIZE]);

    private StreamUtil() {
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] buffer = BUFFER_HOLDER.get();
        try {
            int readCount;
            while (true) {
                readCount = inputStream.read(buffer);
                if (readCount == -1) {
                    break;
                }
                outputStream.write(buffer, 0, readCount);
            }
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
            try {
                outputStream.close();
            } catch (IOException e) {
                log.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }
}
