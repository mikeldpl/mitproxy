package com.mikeldpl.mitproxy.utils;

import java.util.Set;
import java.util.TreeSet;

public final class HttpUtil {

    private static final String GET = "GET";

    private static final Set<String> TRANSPORT_HEADERS_SET = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    static {
        TRANSPORT_HEADERS_SET.add("Content-Length");
        TRANSPORT_HEADERS_SET.add("Transfer-Encoding");
        TRANSPORT_HEADERS_SET.add("Connection");
        TRANSPORT_HEADERS_SET.add("Content-Encoding");
        TRANSPORT_HEADERS_SET.add("Accept-Encoding");
        TRANSPORT_HEADERS_SET.add("Server");
        TRANSPORT_HEADERS_SET.add("Vary");
        TRANSPORT_HEADERS_SET.add("Host");
        TRANSPORT_HEADERS_SET.add("Keep-Alive");
        TRANSPORT_HEADERS_SET.add("Trailer");
        TRANSPORT_HEADERS_SET.add("Upgrade");
    }

    private HttpUtil() {
    }

    public static boolean isTransportHeader(String header) {
        return TRANSPORT_HEADERS_SET.contains(header);
    }

    public static boolean isMethodGet(String method) {
        return GET.equals(method);
    }
}
