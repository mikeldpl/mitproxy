package com.mikeldpl.mitproxy.servlets;

import com.mikeldpl.mitproxy.configureations.ProxyServerConfiguration;
import com.mikeldpl.mitproxy.forvarders.HttpHostForwarder;
import com.mikeldpl.mitproxy.inspectors.Inspector;
import com.mikeldpl.mitproxy.utils.BeansUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "ProxyServlet", value = "/", loadOnStartup = 0)
public class ProxyServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(ProxyServlet.class.getCanonicalName());
    private HttpHostForwarder httpHostForwarder;
    private Inspector inspector;
    private RequestDispatcher defaultServletDispatcher;

    @Override
    public void init() {
        ServletContext servletContext = getServletContext();
        httpHostForwarder = BeansUtil.getBean(servletContext, HttpHostForwarder.class);
        inspector = BeansUtil.getBean(servletContext, Inspector.class);
        defaultServletDispatcher = BeansUtil.getBean(servletContext, ProxyServerConfiguration.class)
                .getDefaultServletDispatcher();
    }

    @Override
    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            if (inspector.isUnableToForward(req)) {
                httpHostForwarder.forward(req, resp);
            } else {
                defaultServletDispatcher.forward(req, resp);
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Exception in proxy server was thrown: " + ex.getMessage(), ex);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
