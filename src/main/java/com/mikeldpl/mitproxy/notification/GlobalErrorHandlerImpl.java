package com.mikeldpl.mitproxy.notification;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GlobalErrorHandlerImpl implements GlobalErrorHandler {

    private static final String REDIRECT = "redirect:";
    private static final String FORWARD = "forward:";

    private Map<Integer, ErrorHandler> handlers = new HashMap<>();

    public GlobalErrorHandlerImpl() {
    }

    public GlobalErrorHandlerImpl(Map<Integer, String> config) {
        for (Map.Entry<Integer, String> stringStringEntry : config.entrySet()) {
            addHandler(stringStringEntry.getKey(), stringStringEntry.getValue());
        }
    }

    @Override
    public void handleError(int responseCode, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!response.isCommitted()) {
            response.setStatus(responseCode);
            ErrorHandler handler = handlers.get(responseCode);
            if (handler != null) {
                handler.handle(request, response);
            }
        }
    }

    @Override
    public void addHandler(int responseCode, ErrorHandler handler) {
        handlers.put(responseCode, handler);
    }

    @Override
    public void addHandler(int responseCode, String handlerDescription) {
        if (handlerDescription != null) {
            handlers.put(responseCode, handlerFromString(handlerDescription));
        }
    }

    private ErrorHandler handlerFromString(String handlerDescription) {
        if (handlerDescription.startsWith(REDIRECT)) {
            final String path = handlerDescription.substring(REDIRECT.length());
            return (request, response) -> response.sendRedirect(path);
        }
        if (handlerDescription.startsWith(FORWARD)) {
            final String path = handlerDescription.substring(FORWARD.length());
            return (request, response) -> {
                try {
                    request.getRequestDispatcher(path).forward(request, response);
                } catch (ServletException e) {
                    throw new IOException("Invalid forward path:" + path, e);
                }
            };
        }
        return (request, response) -> {
            response.getWriter().write(handlerDescription);
            response.getWriter().close();
        };
    }
}
