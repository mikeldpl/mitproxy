package com.mikeldpl.mitproxy.notification;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface GlobalErrorHandler {

    void handleError(int responseCode, HttpServletRequest request, HttpServletResponse httpServletRequest) throws IOException;

    void addHandler(int responseCode, ErrorHandler handler);

    void addHandler(int responseCode, String handlerDescription);
}
