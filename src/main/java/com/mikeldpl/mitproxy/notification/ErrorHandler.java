package com.mikeldpl.mitproxy.notification;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@FunctionalInterface
public interface ErrorHandler {

    void handle(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
