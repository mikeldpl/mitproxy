package com.mikeldpl.mitproxy.forvarders;

import com.mikeldpl.mitproxy.utils.HttpUtil;
import com.mikeldpl.mitproxy.exceptions.ProxyServerConnectionException;
import com.mikeldpl.mitproxy.exceptions.ProxyServerRequestException;
import com.mikeldpl.mitproxy.utils.StreamUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

public class PersistentConnectionsHttpHostForwarder implements HttpHostForwarder {

    private final boolean useCaches;
    private URL host;

    public PersistentConnectionsHttpHostForwarder(boolean useCaches, URL host) {
        this.useCaches = useCaches;
        this.host = host;
    }

    @Override
    public void forward(HttpServletRequest request, HttpServletResponse response) {
        doRequest(openConnection(request), response);
    }

    @Override
    public void setHost(URL host) {
        this.host = host;
    }

    private HttpURLConnection openConnection(HttpServletRequest request) {
        try {
            HttpURLConnection connection = createConnectionObject(request);
            connection.setRequestMethod(request.getMethod());
            connection.setUseCaches(useCaches);

            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                if (!HttpUtil.isTransportHeader(key)) {
                    Enumeration<String> headersValues = request.getHeaders(key);
                    while (headersValues.hasMoreElements()) {
                        connection.addRequestProperty(key, headersValues.nextElement());
                    }
                }
            }

            if (HttpUtil.isMethodGet(request.getMethod())) {
                connection.connect();
            } else {
                connection.setDoOutput(true);
                StreamUtil.copy(request.getInputStream(), connection.getOutputStream());
            }

            return connection;
        } catch (IOException e) {
            throw new ProxyServerConnectionException("Can't create connection", e);
        }
    }

    private HttpURLConnection createConnectionObject(HttpServletRequest request) throws IOException {
        String queryString = request.getQueryString();
        return (HttpURLConnection) new URL(host.getProtocol(), host.getHost(), host.getPort(),
                host.getPath() + request.getServletPath() + (queryString != null ? ("?" + queryString) : "")).openConnection();
    }

    private void doRequest(HttpURLConnection connection, HttpServletResponse response) {
        try {

            InputStream inputStream;
            try {
                inputStream = connection.getInputStream();
            } catch (IOException e) {
                inputStream = connection.getErrorStream();
            }

            response.setStatus(connection.getResponseCode());
            for (Map.Entry<String, List<String>> stringListEntry : connection.getHeaderFields().entrySet()) {
                String key = stringListEntry.getKey();
                if (key != null && !HttpUtil.isTransportHeader(key)) {
                    for (String value : stringListEntry.getValue()) {
                        response.addHeader(key, value);
                    }
                }
            }

            StreamUtil.copy(inputStream, response.getOutputStream());

        } catch (IOException e) {
            throw new ProxyServerRequestException("Can't do request to internal server", e);
        }
    }
}
