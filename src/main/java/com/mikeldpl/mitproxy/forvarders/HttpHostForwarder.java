package com.mikeldpl.mitproxy.forvarders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;

public interface HttpHostForwarder {

    void forward(HttpServletRequest request, HttpServletResponse response);

    void setHost(URL host);
}
