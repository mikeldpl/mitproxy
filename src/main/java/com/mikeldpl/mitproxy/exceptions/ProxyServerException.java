package com.mikeldpl.mitproxy.exceptions;

public class ProxyServerException extends RuntimeException {

    public ProxyServerException(String message) {
        super(message);
    }

    public ProxyServerException(String message, Throwable cause) {
        super(message, cause);
    }

}
