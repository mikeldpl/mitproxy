package com.mikeldpl.mitproxy.exceptions;

public class ProxyServerRequestException extends ProxyServerException {

    public ProxyServerRequestException(String message, Throwable cause) {
        super(message, cause);
    }

}
