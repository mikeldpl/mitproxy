package com.mikeldpl.mitproxy.exceptions;

public class ProxyServerConfigurationException extends ProxyServerException {

    public ProxyServerConfigurationException(String message) {
        super(message);
    }

    public ProxyServerConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

}
