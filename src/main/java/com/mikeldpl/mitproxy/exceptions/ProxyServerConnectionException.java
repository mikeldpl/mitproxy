package com.mikeldpl.mitproxy.exceptions;

public class ProxyServerConnectionException extends ProxyServerException {

    public ProxyServerConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

}
