package com.mikeldpl.mitproxy.filters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "SessionDisabledFiler", urlPatterns = "/*")
public class SessionDisabledFiler extends AbstractHttpFilter {

    @Override
    protected void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        chain.doFilter(new DisabledHttpServletRequestWrapper(request), response);
    }

    private static class DisabledHttpServletRequestWrapper extends HttpServletRequestWrapper {
        private static final String HTTP_SESSION_IS_UNSUPPORTED = "Http session is unsupported";

        public DisabledHttpServletRequestWrapper(HttpServletRequest request) {
            super(request);
        }

        @Override
        public String getRequestedSessionId() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public HttpSession getSession(boolean create) {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public HttpSession getSession() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public String changeSessionId() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public boolean isRequestedSessionIdValid() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public boolean isRequestedSessionIdFromCookie() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public boolean isRequestedSessionIdFromURL() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }

        @Override
        public boolean isRequestedSessionIdFromUrl() {
            throw new UnsupportedOperationException(HTTP_SESSION_IS_UNSUPPORTED);
        }
    }
}
