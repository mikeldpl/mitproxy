package com.mikeldpl.mitproxy.filters;

import com.mikeldpl.mitproxy.notification.GlobalErrorHandler;
import com.mikeldpl.mitproxy.utils.BeansUtil;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

@WebFilter(filterName = "ErrorPageFilter", urlPatterns = "/*")
public class ErrorPageFilter extends AbstractHttpFilter {

    private GlobalErrorHandler globalErrorHandler;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        globalErrorHandler = BeansUtil.getBean(filterConfig.getServletContext(), GlobalErrorHandler.class);
    }

    @Override
    protected void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        chain.doFilter(request, new ErrorPageHttpServletResponseWrapper(request, response));
    }

    private class ErrorPageHttpServletResponseWrapper extends HttpServletResponseWrapper {
        private HttpServletRequest request;

        public ErrorPageHttpServletResponseWrapper(HttpServletRequest request, HttpServletResponse response) {
            super(response);
            this.request = request;
        }

        @Override
        public void sendError(int sc) throws IOException {
            globalErrorHandler.handleError(sc, request, this);
        }

        @Override
        public void sendError(int sc, String msg) throws IOException {
            sendError(sc);
        }
    }
}
