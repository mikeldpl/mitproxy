package com.mikeldpl.mitproxy.inspectors;

import javax.servlet.http.HttpServletRequest;

public interface Inspector {
    boolean isUnableToForward(HttpServletRequest request);
}
