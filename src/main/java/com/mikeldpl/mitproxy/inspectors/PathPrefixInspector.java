package com.mikeldpl.mitproxy.inspectors;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class PathPrefixInspector implements Inspector {

    private Set<String> pathsPrefixes;

    public PathPrefixInspector(Set<String> pathsPrefixes) {
        this.pathsPrefixes = pathsPrefixes;
    }

    @Override
    public boolean isUnableToForward(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        int nextSlashIndex = servletPath.indexOf('/', 1);
        return pathsPrefixes.contains(servletPath.substring(0, nextSlashIndex < 0 ? servletPath.length() : nextSlashIndex));
    }
}
