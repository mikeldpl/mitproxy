package com.mikeldpl.mitproxy.configureations;

import com.mikeldpl.mitproxy.exceptions.ProxyServerConfigurationException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

public class ProxyServerConfigurationImpl implements ProxyServerConfiguration {

    private static final String INTERNAL_SERVER_URL_KEY =
            "com.mikeldpl.mitproxy.configuration.internalServerUrl";
    private static final String USE_CACHES = "com.mikeldpl.mitproxy.configuration.use.caches";
    private static final String MAX_CONNECTIONS_KEY = "com.mikeldpl.mitproxy.configuration.http.maxConnections";
    private static final String ERROR_HANDLER_PREFIX = "com.mikeldpl.mitproxy.configuration.handle.error.";
    private static final String MAX_CONNECTIONS_SYSTEM_PROPERTY = "http.maxConnections";
    private static final String ALLOW_RESTRICTED_HEADERS_SYSTEM_PROPERTY = "sun.net.http.allowRestrictedHeaders";

    private static final String[] SUPPORTED_PROTOCOLS = {"http", "https"};

    private static final String ENABLED_PREFIX_PATHS_FILE = "/enabled_prefix_paths.txt";
    private static final String PROPERTIES_FILE = "/defaultConfiguration.properties";
    private static final String JNDI_LOOKUP_ROOT = "java:comp/env";

    private final URL internalServerUrl;
    private final RequestDispatcher defaultServletDispatcher;
    private final boolean useCaches;
    private final Set<String> pathsPrefixes;
    private Map<Integer, String> errorsHandlerConfiguration;

    public ProxyServerConfigurationImpl(ServletContext ctx) {
        try {
            PropertyResolver propertyResolver = new PropertyResolver();
            internalServerUrl = new URL(propertyResolver.getProperty(INTERNAL_SERVER_URL_KEY));
            useCaches = Boolean.parseBoolean(propertyResolver.getProperty(USE_CACHES));
            defaultServletDispatcher = getDefaultServletDispatcher(ctx);
            pathsPrefixes = getPathsPrefixesSetFromFile();
            errorsHandlerConfiguration = convertKeysToInt(propertyResolver.getByPrefix(ERROR_HANDLER_PREFIX));
            setMaxConnection(propertyResolver);
            System.setProperty(ALLOW_RESTRICTED_HEADERS_SYSTEM_PROPERTY, "true");
            checkProtocol();
        } catch (NamingException | IOException e) {
            throw new ProxyServerConfigurationException("Can't init configuration", e);
        }
    }

    private void setMaxConnection(PropertyResolver propertyResolver) {
        String maxConnections = propertyResolver.getProperty(MAX_CONNECTIONS_KEY);
        if (maxConnections != null) {
            System.setProperty(MAX_CONNECTIONS_SYSTEM_PROPERTY, maxConnections);
        }
    }

    private Map<Integer, String> convertKeysToInt(Map<String, String> map) {
        Map<Integer, String> res = new HashMap<>();
        for (Map.Entry<String, String> stringStringEntry : map.entrySet()) {
            res.put(Integer.valueOf(stringStringEntry.getKey()), stringStringEntry.getValue());
        }
        return res;
    }

    private void checkProtocol() {
        if (!Arrays.stream(SUPPORTED_PROTOCOLS).anyMatch(s -> s.equals(internalServerUrl.getProtocol()))) {
            throw new ProxyServerConfigurationException("Incorrect URL was get " + internalServerUrl
                    + ". It must have some of next protocols: " + Arrays.toString(SUPPORTED_PROTOCOLS));
        }
    }

    private Set<String> getPathsPrefixesSetFromFile() throws IOException {
        Set<String> set = new HashSet<>();
        try (Scanner scanner = new Scanner(new InputStreamReader(
                getClass().getResourceAsStream(ENABLED_PREFIX_PATHS_FILE), "UTF-8"))) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(!line.isEmpty() && !line.startsWith("#")) {
                    set.add(line);
                }
            }
        }
        return set;
    }

    private RequestDispatcher getDefaultServletDispatcher(ServletContext servletContext) {
        String COMMON_DEFAULT_SERVLET_NAME = "default";
        String GAE_DEFAULT_SERVLET_NAME = "_ah_default";
        String RESIN_DEFAULT_SERVLET_NAME = "resin-file";
        String WEBLOGIC_DEFAULT_SERVLET_NAME = "FileServlet";
        String WEBSPHERE_DEFAULT_SERVLET_NAME = "SimpleFileServlet";
        String name;
        if (servletContext.getNamedDispatcher(COMMON_DEFAULT_SERVLET_NAME) != null) {
            name = COMMON_DEFAULT_SERVLET_NAME;
        } else if (servletContext.getNamedDispatcher(GAE_DEFAULT_SERVLET_NAME) != null) {
            name = GAE_DEFAULT_SERVLET_NAME;
        } else if (servletContext.getNamedDispatcher(RESIN_DEFAULT_SERVLET_NAME) != null) {
            name = RESIN_DEFAULT_SERVLET_NAME;
        } else if (servletContext.getNamedDispatcher(WEBLOGIC_DEFAULT_SERVLET_NAME) != null) {
            name = WEBLOGIC_DEFAULT_SERVLET_NAME;
        } else if (servletContext.getNamedDispatcher(WEBSPHERE_DEFAULT_SERVLET_NAME) != null) {
            name = WEBSPHERE_DEFAULT_SERVLET_NAME;
        } else {
            throw new ProxyServerConfigurationException("Can't init configuration. Default servlet wasn't set.");
        }
        return servletContext.getNamedDispatcher(name);

    }

    @Override
    public URL getInternalServerUrl() {
        return internalServerUrl;
    }

    @Override
    public RequestDispatcher getDefaultServletDispatcher() {
        return defaultServletDispatcher;
    }

    @Override
    public boolean isUsingCache() {
        return useCaches;
    }

    @Override
    public Set<String> getPathPrefixes() {
        return pathsPrefixes;
    }

    @Override
    public Map<Integer, String> getErrorsHandlerConfiguration() {
        return errorsHandlerConfiguration;
    }

    private static class PropertyResolver {
        private Context context;
        private Properties prop;

        public PropertyResolver() throws IOException, NamingException {
            prop = new Properties();
            try (InputStream inputStream = getClass().getResourceAsStream(PROPERTIES_FILE)) {
                prop.load(inputStream);
            }
            context = (Context) new InitialContext().lookup(JNDI_LOOKUP_ROOT);
        }

        @SuppressWarnings("PMD.EmptyCatchBlock")
        public String getProperty(String name) {
            try {
                return (String) context.lookup(name);
            } catch (NamingException e) {
                //it is normal behaviour
            }
            return prop.getProperty(name);
        }

        public Map<String, String> getByPrefix(String prefix) {
            Map<String, String> res = new HashMap<>();
            for (String s : prop.stringPropertyNames()) {
                if (s.startsWith(prefix)) {
                    res.put(s.substring(prefix.length()), prop.getProperty(s));
                }
            }
            return res;
        }
    }

}
