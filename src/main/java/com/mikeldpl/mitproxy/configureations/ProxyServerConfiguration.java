package com.mikeldpl.mitproxy.configureations;

import javax.servlet.RequestDispatcher;
import java.net.URL;
import java.util.Map;
import java.util.Set;

public interface ProxyServerConfiguration {
    URL getInternalServerUrl();

    RequestDispatcher getDefaultServletDispatcher();

    boolean isUsingCache();

    Set<String> getPathPrefixes();

    Map<Integer, String> getErrorsHandlerConfiguration();
}
